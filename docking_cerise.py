from typing import List, Tuple
from ihm.coordinate_system import Position
from ihm.strat import TeamColor

from math import pi

RAYON_KOBE = 120 + 16           # Non c'est pas une blague, il fait 12cm de rayon (interne)
DISTANCE_CADRE_CHARLES = 140    # Distance en X ou en Y entre le bord du plateau et le centre du robot en utilisant le cadre
ROTATION_TUBE = pi / 2          # Rotation entre la face avant et la face du tube

MARGE_KOBE = 3                  # Marge à ajouter pour que Kobe ne se prenne pas les mur
LARGEUR_RACK = 300
HAUTEUR_RACK = 30

# Constantes, aucune obiligation quand à leur utilisation
POS_DEPART_CADRE_BLEU = Position(DISTANCE_CADRE_CHARLES, DISTANCE_CADRE_CHARLES, ROTATION_TUBE) 
POS_DEPART_CADRE_VERT = Position(DISTANCE_CADRE_CHARLES, DISTANCE_CADRE_CHARLES, 0) 

Trajet = List[Position]

def _trajet_rack (rack: int) -> Trajet:

    depart = Position(0, 0, 0)
    arrive = Position(0, 0, 0)
    inter = Position(0, 0, 0)

    if rack == 0:
        depart.x = 3000 - MARGE_KOBE - RAYON_KOBE
        depart.y = 1000 - MARGE_KOBE - (HAUTEUR_RACK / 2) - RAYON_KOBE 

        arrive.x = 3000 -  LARGEUR_RACK - MARGE_KOBE - RAYON_KOBE
        arrive.y = 1000 - MARGE_KOBE- (HAUTEUR_RACK / 2) - RAYON_KOBE

        depart.teta = pi 
        arrive.teta = pi 

    elif rack == 1:
        depart.x = 1500 - ( LARGEUR_RACK / 2) - MARGE_KOBE + RAYON_KOBE
        depart.y = 0 + MARGE_KOBE + RAYON_KOBE + HAUTEUR_RACK
        
        arrive.x = 1500 + ( LARGEUR_RACK / 2) + MARGE_KOBE + RAYON_KOBE
        arrive.y = 0 + MARGE_KOBE + RAYON_KOBE + HAUTEUR_RACK

        depart.teta = 0 
        arrive.teta = 0 

    elif rack == 2:
        depart.x = 1500 + ( LARGEUR_RACK / 2) + MARGE_KOBE - RAYON_KOBE
        depart.y = 2000 - MARGE_KOBE - RAYON_KOBE - HAUTEUR_RACK
        
        arrive.x = 1500 - ( LARGEUR_RACK / 2) - MARGE_KOBE - RAYON_KOBE
        arrive.y = 2000 - MARGE_KOBE - RAYON_KOBE - HAUTEUR_RACK

        depart.teta = pi 
        arrive.teta = pi 

    elif rack == 3:
        depart.x = 0 + MARGE_KOBE + RAYON_KOBE
        depart.y = 1000 + MARGE_KOBE + RAYON_KOBE + (HAUTEUR_RACK / 2)

        arrive.x = 0 +  LARGEUR_RACK + MARGE_KOBE + RAYON_KOBE
        arrive.y = 1000 + MARGE_KOBE + RAYON_KOBE + (HAUTEUR_RACK / 2)

        depart.teta = 0 
        arrive.teta = 0 

    inter.x = (depart.x + arrive.x) / 2
    inter.y = (depart.y + arrive.y) / 2
    inter.teta = depart.teta

    return [depart, inter, arrive]

aspire_racks_LUT: List[Trajet] = [
    # Rack 0 - Celui à "gauche", au bout du terrain [3000, 1000]
    _trajet_rack(0),
    # Rack 1 - Celui en "bas" du plateau [1500, 0]
    _trajet_rack(1),
    # Rack 2 - Celui en "haut" du plateau [1500, 2000]
    _trajet_rack(2),
    # Rack 3 - Celui à "droite" du plateau [0, 1000]
    _trajet_rack(3),
]

# Retourne la route à suivre pour aspirer les cerises 
def aspire_rack (rack: int) -> Trajet:
    return aspire_racks_LUT[rack]


# Donne la route à suivre avant aspiration, evite les collisions
def route_rack (depart: Position, rack: int) -> Trajet:
    traj = []
    
    arrive = aspire_rack(rack)[0]

    if rack == 0:
        # On sait que l'arrivée est en dessous du bloc
        if depart.y > 1500:
            traj.append(Position(3000 - LARGEUR_RACK - MARGE_KOBE - RAYON_KOBE, 1000 + HAUTEUR_RACK + RAYON_KOBE, 0))
            traj.append(Position(3000 - LARGEUR_RACK - MARGE_KOBE - RAYON_KOBE, 1000 - HAUTEUR_RACK - RAYON_KOBE, 0))

    if rack == 3:
        # On sait que l'arrivée est au dessus du bloc
        if depart.y < 1500:
            traj.append(Position(LARGEUR_RACK + MARGE_KOBE + RAYON_KOBE, 1000 - HAUTEUR_RACK - RAYON_KOBE, 0))
            traj.append(Position(LARGEUR_RACK + MARGE_KOBE + RAYON_KOBE, 1000 + HAUTEUR_RACK + RAYON_KOBE, 0))

    traj.append(arrive)

    return traj


def _prepa_retour_maison (depart: Position, equipe: TeamColor) -> Trajet:
    trajet = []

    if equipe == TeamColor.BLEU:

        # On est du mauvais côté 
        if depart.y > 1000:
            
            # On est au dessus du rack 3
            if depart.x < LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 30:
                trajet.append(Position(LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 100, depart.y, depart.teta))
                trajet.append(Position(LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 100, 1000 - RAYON_KOBE - HAUTEUR_RACK - MARGE_KOBE - 50, 0))

            # On est au dessus du rack 0
            elif depart.x > (3000 - LARGEUR_RACK - RAYON_KOBE - MARGE_KOBE - 30):
                trajet.append(Position(3000 - (LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 100), depart.y, depart.teta))
                trajet.append(Position(3000 - (LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 100), 1000 - RAYON_KOBE - HAUTEUR_RACK - MARGE_KOBE - 50, 0))

            # On est dans la zone "libre"
            else:
                trajet.append(Position(depart.x, 1000 - RAYON_KOBE - HAUTEUR_RACK - MARGE_KOBE - 100, 0))

    else:

        # On est du mauvais côté 
        if depart.y < 1000:
            
            # On est en dessous du rack 3
            if depart.x < LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 30:
                trajet.append(Position(LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 100, depart.y, depart.teta))
                trajet.append(Position(LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 100, 1000 + RAYON_KOBE + HAUTEUR_RACK + MARGE_KOBE + 50, 0))

            # On est en dessous du rack 0
            elif depart.x > (3000 - LARGEUR_RACK - RAYON_KOBE - MARGE_KOBE - 30):
                trajet.append(Position(3000 - (LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 100), depart.y, depart.teta))
                trajet.append(Position(3000 - (LARGEUR_RACK + RAYON_KOBE + MARGE_KOBE + 100), 1000 + RAYON_KOBE + HAUTEUR_RACK + MARGE_KOBE + 50, 0))

            # On est dans la zone "libre"
            else:
                trajet.append(Position(depart.x, 1000 + RAYON_KOBE + HAUTEUR_RACK + MARGE_KOBE + 100, 0))

    return trajet


def retour_maison (depart: Position, equipe: TeamColor) -> List[Tuple]:
    pre_trajet = _prepa_retour_maison(depart, equipe)
    
    instructions = []
    
    for pos in pre_trajet:
        instructions.append(("pos", pos))

    if equipe == TeamColor.BLEU:
        # On bourre le côté du plateau
        instructions.append(("pos", Position(RAYON_KOBE + MARGE_KOBE, RAYON_KOBE + MARGE_KOBE, pi/2)))
        instructions.append(("trap", True))
        instructions.append(("pos", Position(RAYON_KOBE + MARGE_KOBE + 10, RAYON_KOBE + MARGE_KOBE, pi/2)))
        instructions.append(("wait", 5))
        instructions.append(("trap", False))
        instructions.append(("pos", Position(RAYON_KOBE + MARGE_KOBE, RAYON_KOBE + MARGE_KOBE, pi/2)))
    
    else:
        # On bourre le côté du plateau
        instructions.append(("pos", Position(RAYON_KOBE + MARGE_KOBE, 2000 - (RAYON_KOBE + MARGE_KOBE), pi/2)))
        instructions.append(("trap", True))
        instructions.append(("pos", Position(RAYON_KOBE + MARGE_KOBE + 10, 2000 - (RAYON_KOBE + MARGE_KOBE), pi/2)))
        instructions.append(("wait", 5))
        instructions.append(("trap", False))
        instructions.append(("pos", Position(RAYON_KOBE + MARGE_KOBE, 2000 - (RAYON_KOBE + MARGE_KOBE), pi/2)))
    
    return instructions

# Retourne le poin où se trouve le robot au final
def attrape_cerise (depart: Position, rack: int) -> Tuple[List[Tuple], Position]:
    trajet = route_rack(depart, rack)
    aspiration = aspire_rack(rack)

    instructions = []

    for pos in trajet:
        instructions.append(("pos", pos))

    instructions.append(("esc", True))
    instructions.append(('pos', aspiration[1]))
    instructions.append(('pos', aspiration[2]))
    instructions.append(("esc", False))

    return instructions, aspiration[2]

