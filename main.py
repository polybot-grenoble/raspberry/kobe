from ihm import WindowManager, IMAGES, \
    MainMenuPage, SuperviseurPage, BaseMobilePage, LidarPage, OdometriePage, StratPage, CameraPage, VitalsPage, ConfigManagerPage
from ihm.strat import StrategieManager, Tirette
from ihm.coordinate_system import Position
from ihm.client_mqtt import client, robot1, DataFrame

from strategies import STRAT_DICT

from ihm.stm import StmManager

from ihm.posManager import PosManager
from ihm.obstacleManager import ObstacleManager

TIRETTE_PIN = 4


_pos_manager = PosManager()
_obstacle_manager = ObstacleManager()


Tirette(TIRETTE_PIN)
StrategieManager(STRAT_DICT)

window_manager = WindowManager()

window_manager.build_menu_bar(
    [
        (IMAGES.MENU.HOME, MainMenuPage),
        (IMAGES.MENU.PLAY, StratPage),
        (IMAGES.MENU.LIDAR, LidarPage),
        (IMAGES.MENU.CAMERA, [
            ("Caméra", CameraPage),
            ("Prévisualisation", SuperviseurPage),
            ("Performances", VitalsPage)
        ]),
        (IMAGES.MENU.WHEEL, [
            ("Odométrie", OdometriePage),
            ("Fichier Configuration", ConfigManagerPage),
        ]),
        (IMAGES.MENU.OFF, WindowManager.validate_destruction),
    ]
)

MainMenuPage.display()

def on_command(trame:DataFrame):
    match trame.command:
        case "pos_get":
            pos = PosManager.pos_manager.position
            client.sendResponse(trame.sender, "pos_get", [str(pos.x),str(pos.y), str(pos.teta)])

        case "goto":
            coords = trame.data
            target = Position(float(coords[0]), float(coords[1]), float(coords[2]))
            PosManager.pos_manager.go_to(target)

        case "lidar_get" | "debug_lidar_stream":
            pass


        case _:
            pass
robot1.on_received_message = on_command


# stm_manager.go_to(Position(100, 100, 0))

window_manager.main_loop()
client.stop()