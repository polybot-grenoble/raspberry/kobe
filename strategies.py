from abc import ABC, abstractmethod
from time import sleep
from typing import Dict, Type, Iterable, Tuple
from numbers import Number
from threading import Thread
from time import sleep
from math import pi
import logging
from math import pi

from ihm.utils import TimeOut, wait_until
from ihm import stm, client_mqtt  # , lidar
from ihm.strat import Strategie, StrategieManager

from ihm.stm import StmManager, coordinate_system
from ihm.coordinate_system import Position
from ihm.client_mqtt import Superviseur

from ihm.strat import TeamColor

from ihm.posManager import PosManager

from docking_cerise import attrape_cerise, retour_maison

POSX_depart = 200.0
POSY_depart = 200.0

class StrategieWithSuperviseur(Strategie, ABC):
    def __init__(self, team_color: TeamColor, aruco: Dict[str, int], start_position: Position) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """
        Superviseur.superviseur.stream_sync()
        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._start_position = start_position

    def setup(self) -> None:
        StrategieManager.strategie_manager.set_state("Position de départ")
        logging.info("En attente du superviseur")
        wait_until(lambda: PosManager.pos_manager.isSynced)
        logging.info("Deplacement vers la position intermediaire")
        PosManager.pos_manager.go_to(Position(1000, 1000, 0))
        wait_until(lambda: PosManager.pos_manager.arrived)
        logging.info("Deplacement vers la position de départ")
        PosManager.pos_manager.go_to(self._start_position)

    def abort(self):
        Superviseur.superviseur.stream_stop()
        PosManager.pos_manager.stop()

    @abstractmethod
    def get_start_position(self) -> Position:
        pass


class TestStrat(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._updated = False
        self.commands = [
            ("grab", (180, 180, 180, 180)),
            ("pos", Position(200  , 1500, 0)),
            ("pos", Position(1500, 1500, 0)),
            ("pos", Position(1500, 200  , 0)),
            ("pos", Position(200  , 200  , 0)),
            ("grab", (90, 90, 90, 90)),
            ("wait", 3),


            ("pos", Position(1500, 1500, 0)),
            ("pos", Position(200  , 1500, 0)),
            ("pos", Position(1500, 200  , 0)),
            ("pos", Position(200  , 200  , 0)),


            # ESC and Trapdoor usage
            # ("esc", True),
            # ("wait", 3),
            # ("esc", False),
            # ("wait", 3),

            # ("trap", True),
            # ("wait", 3),
            # ("trap", False),
            # ("wait", 3),

        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case "grab" | "wait":
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass


class BlueSuckStrat(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        #super().__init__(team_color, aruco, Position(200, 200, 0))

        self._updated = False
        self.commands = [
            ("pos", Position(800, 800, 0)),
            ("wait", 1),
            ("pos", Position(POSX_depart, POSY_depart, pi/2)),
            ("pos", Position(POSX_depart-200, POSX_depart, pi/2)),
            ("trap", True),
        ]

        self.commands += [("pos", Position(POSX_depart-200, POSY_depart-50, pi/2)), ("pos", Position(POSX_depart-200, POSY_depart-50, pi/2))]*5
        self.commands += [("wait", 1)]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])
                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case _:
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass

class BlueEasyStrat(Strategie):
    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        #super().__init__(team_color, aruco, Position(1000, 1000, 0))

        self._updated = False
        self.commands = [
            ("pos", Position(1650, 200, 0)),
            ("pos", Position(1650, 140, 0)),
            ("esc", True),
            ("wait", 1),
            ("pos", Position(1350, 180, 0)),
            ("pos", Position(1350, 220, 0)),
            ("wait", 1),
            ("esc", False),
            ("pos", Position(POSX_depart, POSY_depart, pi/2)),
            ("pos", Position(POSX_depart-200, POSY_depart , pi/2)),
            ("trap", True)
        ]

        self.commands += [("pos", Position(POSX_depart-200, POSY_depart-50, pi/2)), ("pos", Position(POSX_depart-200, POSY_depart-50, pi/2))]*5
        self.commands += [("wait", 1)]

    def get_start_position(self):
        return Position(POSX_depart, POSY_depart, 0)
   
    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case _:
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass


class GreenEasyStrat(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        #super().__init__(team_color, aruco, Position(225, 2000-225, 0))

        self._updated = False
        self.commands = [
            ("pos", Position(1650, POSY_depart, pi)),
            ("pos", Position(1650, 220, pi)),
            ("esc", True),
            ("wait", 1),
            ("pos", Position(1350, 220, pi)),
            ("pos", Position(1350, 180, pi)),
            ("wait", 1),
            ("esc", False),
            ("pos", Position(POSX_depart, POSY_depart, pi/2)),
            ("pos", Position(POSX_depart+100, POSY_depart , pi/2)),
            ("trap", True)
        ]

        self.commands += [("pos", Position(POSX_depart-200, POSY_depart-50, pi/2)), ("pos", Position(POSX_depart-200, POSY_depart-50, pi/2))]*5
        self.commands += [("wait", 1)]

        pass

    def get_start_position(self):
        return Position(POSX_depart, POSY_depart, 0)

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case _:
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass

class ListedStrat(Strategie, ABC):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        self._main_loop_thread = None

        self._team_color = team_color
        self._aruco = aruco

        self._updated = False
        self.commands = self._get_commands()

    @abstractmethod
    def _get_commands(self) -> Iterable[Tuple]:
        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command):
                case ("pos", position):
                    PosManager.pos_manager.go_to(position)

                case ("grab", angle_iterable):
                    StmManager.stm_manager.set_grippers(angle_iterable)
                    sleep(0.2)

                case ("esc", on_off):
                    StmManager.stm_manager.set_ESC(on_off)

                case ("trap", open_close):
                    StmManager.stm_manager.set_Trapdoor(open_close)

                case ("wait", delai_seconds):
                    sleep(delai_seconds)

                case ("set_pos", position):
                    StmManager.stm_manager.set_position(position)

                case _:
                    logging.warning("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command):
                case ("pos", _):
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return

                case _:
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        StmManager.stm_manager.set_position(self.get_start_position())
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        PosManager.pos_manager.stop()

    @abstractmethod
    def score(self) -> int:
        pass

    @abstractmethod
    def get_start_position(self) -> Position:
        pass

    def setup(self) -> None:
        PosManager.pos_manager.init_cale_kobe_go_to(self._team_color, self.get_start_position())


class PreloadedOnlyBlue(ListedStrat):
    def get_start_position(self):
        return Position(200, 200, pi/2)

    def _get_commands(self) -> Iterable[Tuple]:
        return [
            ("pos", Position(800, 800, 0)),
            ("wait", 1),
            ("pos", Position(POSX_depart, POSY_depart, pi/2)),
            ("pos", Position(0, POSY_depart+80, pi/2)),
            ("trap", True),
            ("wait",5),
            ("pos", Position(20, POSY_depart+80, pi/2)),
            ("wait",1)
        ]
    
    def score(self) -> int:
        return 43  # Score estim�


class PreloadedOnlyGreen(ListedStrat):
    def get_start_position(self):
        return Position(200, 1800, 0)

    def _get_commands(self) -> Iterable[Tuple]:
        return [
            ("pos", Position(800, 1200, 0)),
            ("wait", 1),
            ("pos", Position(200, 1800, pi/2)),
            ("pos", Position(0, 1720, pi/2)),
            ("trap", True),
            ("wait",5),
            ("pos", Position(20, 1720, pi/2)),
            ("wait",1)
        ]
    
    def score(self) -> int:
        return 43  # Score estim�


class BlueCenterStrat(ListedStrat): # KOBEEE !!!!!!!!!!
    # Position initiale : centree sur l'assiette du milieu (x=1875, y=210), aspirateur vers le mur
    def _get_commands(self):
        return [
            ("esc", True), # Turbine on
            ("pos", ( 1350, 210, 0)), # Ramasse cerises
            ("esc", False), # Turbine off
            ("pos", ( 1350, 210, -pi/2)), # Rotation pour aller deposer les cerises
            ("pos", ( 25, 325, 0)), # Depos des cerises
            ("trap", True),
            ("wait", 5),
            ("trap", False),
            ("pos", ( -5, -5, 0)) # Les pieds dans le plats !
        ]

    def score(self) -> int:
        return 43  # Score estim�
    
    def get_start_position(self):
        return Position(1875, 210, 0)

class GreenCenterStrat(ListedStrat): # KOBEEE !!!!!!!!!!
    # Position initiale : centree sur l'assiette du milieu (x=1875, y=1790), aspirateur vers le mur
    def _get_commands(self):
        return [
            ("esc", True), # Turbine on
            ("pos", ( 1350, 1790, 0)), # Ramasse cerises
            ("esc", False), # Turbine off
            ("pos", ( 1350, 1790, -pi/2)), # Rotation pour aller deposer les cerises
            ("pos", ( 25, 1675, 0)), # Depos des cerises
            ("trap", True),
            ("wait", 5),
            ("trap", False),
            ("pos", ( -5, 2005, 0)) # Les pieds dans le plats !
        ]

    def score(self) -> int:
        return 43  # Score estim�
    
    def get_start_position(self):
        return Position(1875, 1790, 0)


class BlueAllStraightAction(ListedStrat):
    def get_start_position(self) -> Position:
        return Position(300, 225, pi/4)

    def _get_commands(self) -> Iterable[Tuple]:
        return [
            ("pos", Position(1700, 225, pi/4)),  # Pousser le(s) premier gateau dans l'assiette
            ("pos", Position(1500, 225, 0)),  # Se mettre en face des cerises
            ("pos", Position(1500, 100, 0)),  # Se rapprocher des cerises
            ("set_pos", Position(1500, 145 + 30, 0)),
            ("pos", Position(1500 + 300/2 + 70 + 30, 145 + 30, 0)),  # Se mettre avant les cerises
            ("esc", True),
            ("pos", Position(1500 - 300/2 + 70 - 30, 145 + 30, 0)),  # Aller jusqu'à la fin des cerises
            ("esc", False),
            ("pos", Position(165, 225, pi/2)),  # Proche du panier
            ("pos", Position(100, 225, pi/2)),  # Contact au panier
            ("trap", True),
            ("wait", 2),
            ("pos", Position(95, 230, pi/2)),  # Wiggle
            ("pos", Position(100, 220, pi/2)),  # Wiggle
            ("pos", Position(90, 230, pi/2)),  # Wiggle
            ("set_pos", Position(120, 230, 0)),
            ("wait", 6),
            ("trap", False),
            ("pos", Position(120, 145, pi/2)),  # Aller bien dans le coin pour laisser la place à walter
            ("pos", Position(105, 130, pi/2)),  # Aller bien dans le coin pour laisser la place à walter
            ("wait", 1),
        ]

    def score(self) -> int:
        points_gateau = 3
        points_cerises = 20
        points_comptage_cerises = 5
        # points de sortie négligé (manque probable d'une cerise)
        return points_gateau + points_cerises + points_comptage_cerises


class GreenAllStraightAction(ListedStrat):
    def get_start_position(self) -> Position:
        return Position(300, 2000-225, pi/4)

    def _get_commands(self) -> Iterable[Tuple]:
        return [
            ("pos", Position(1700, 2000-225, pi/4)),  # Pousser le(s) premier gateau dans l'assiette
            ("pos", Position(1500, 2000-225, pi)),  # Se mettre en face des cerises
            ("pos", Position(1500, 2000-100, pi)),  # Se rapprocher des cerises
            ("set_pos", Position(1500, 2000-(145 + 30), pi)),
            ("pos", Position(1500 + 300/2 + 70 + 30, 30+2000-145, pi)),  # Se mettre avant les cerises
            ("esc", True),
            ("pos", Position(1500 - 300/2 + 70 - 40, 30+2000-145, pi)),  # Aller jusqu'à la fin des cerises
            ("esc", False),
            ("pos", Position(165, 2000-225, pi/2)),  # Proche du panier
            ("pos", Position(50, 2000-225, pi/2)),  # Contact au panier
            ("trap", True),
            ("wait", 2),
            ("pos", Position(50, 2000-230, pi/2)),  # Wiggle
            ("pos", Position(60, 2000-220, pi/2)),  # Wiggle
            ("pos", Position(50, 2000-230, pi/2)),  # Wiggle
            ("set_pos", Position(120, 2000-230, pi)),
            ("wait", 6),
            ("trap", False),
            ("pos", Position(80, 2000-105, pi/2)),  # Aller bien dans le coin pour laisser la place à walter
            ("pos", Position(65, 2000-110, pi/2)),  # Aller bien dans le coin pour laisser la place à walter
            ("wait", 1),
        ]

    def score(self) -> int:
        points_gateau = 3
        points_cerises = 20
        points_comptage_cerises = 5
        # points de sortie négligé (manque probable d'une cerise)
        return points_gateau + points_cerises + points_comptage_cerises


class HomologationRobot1(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._updated = False
        self.commands = [

            ("pos", Position(950.0, 940.0, 0)),
            ("grab", (90, 90, 90, 180)),
            ("pos", Position(1077.5, 777.5, 0)),
            ("grab", (90, 90, 90, 90)),
            ("pos", Position(1730.0, 350.0, 0)),
            ("grab", (90, 90, 90, 180)),
            
            ("pos", Position(1605.0, 502.5, 0)),
            ("pos", Position(230.0, 502.5, 0)),



            # ("grab", (180, 180, 180, 180)),
            # ("pos", Position(200  , 1500, 0)),
            # ("pos", Position(1500, 1500, 0)),
            # ("pos", Position(1500, 200  , 0)),
            # ("pos", Position(200  , 200  , 0)),
            # ("grab", (90, 90, 90, 90)),
            # ("wait", 3),


            # ("pos", Position(1500, 1500, 0)),
            # ("pos", Position(200  , 1500, 0)),
            # ("pos", Position(1500, 200  , 0)),
            # ("pos", Position(200  , 200  , 0)),


            # ESC and Trapdoor usage
            # ("esc", True),
            # ("wait", 3),
            # ("esc", False),
            # ("wait", 3),

            # ("trap", True),
            # ("wait", 3),
            # ("trap", False),
            # ("wait", 3),

        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case "grab" | "wait":
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass


class TestKobe(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._updated = False
        self.commands = [
            
            ("pos", Position(200  , 1500, 0)),
            ("pos", Position(1500, 1500, 0)),
            ("pos", Position(1500, 200  , 0)),
            ("pos", Position(200  , 200  , 0)),
            
            ("wait", 3),


            #("pos", Position(1500, 1500, 0)),
            #("pos", Position(200  , 1500, 0)),
            #("pos", Position(1500, 200  , 0)),
            #("pos", Position(200  , 200  , 0)),


            # ESC and Trapdoor usage
            ("esc", True),
            ("wait", 3),
            ("esc", False),
            ("wait", 3),

            # ("trap", True),
            # ("wait", 3),
            # ("trap", False),
            # ("wait", 3),

        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case _:
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass

class Demonstrations(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """

        Superviseur.superviseur.team_batch(team_color.value, aruco)
        sleep(0.1)
        Superviseur.superviseur.stream_start()

        self._updated = False
        self.commands = [

            ("pos", Position(310.0, 225.0, 0)),
            # 1er gato chopp�
            # 2e gato chopp�
            ("pos", Position(1875.0, 225.0, 0)),

            #Retour au bercail
            ("pos", Position(POSX_depart, POSY_depart, 0)),
            ("wait", 3),
        ]

        pass

    def update(self):

        if(not self._updated): # update function

            match(self.current_command[0]):
                case "pos":
                    PosManager.pos_manager.go_to(self.current_command[1])

                case "grab":
                    StmManager.stm_manager.set_grippers(self.current_command[1])
                    sleep(0.2)

                case "esc":
                    StmManager.stm_manager.set_ESC(self.current_command[1])

                case "trap":
                    StmManager.stm_manager.set_Trapdoor(self.current_command[1])

                case "wait":
                    sleep(self.current_command[1])

                case _:
                    print("Warning : Unimplemented type of command : {}".format(self.current_command[0]))



            self._updated = True


        match(self.current_command[0]):
                case "pos":
                    if(PosManager.pos_manager.arrived):
                        endCommands = self._next_command()
                        if(endCommands):
                            return
                        
                case "grab" | "wait":
                    endCommands = self._next_command()
                    if(endCommands):
                        return

    def _next_command(self):
        if(len(self.commands) == 0):
            self.abort()
            return True
        self.current_command = self.commands.pop(0)
        self._updated = False
        return False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()
            sleep(0.2)

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        Superviseur.superviseur.stream_stop()
        PosManager.pos_manager.stop()

    def score(self) -> int:
        pass
    pass

### Strat avec calculateur ###
class BlueAutoStrat (ListedStrat): # KOBEEE !!!!!!!!!!
    
    def _get_commands(self):

        pos_0 = Position(1750, 225, pi/4)
        pos_1 = Position(1650, 225, pi/4)
        inst_attrape, pos_2 = attrape_cerise(pos_1, 1)
        inst_maison = retour_maison(pos_1, TeamColor.BLEU)

        instructions = [
            ("pos", pos_0),
            ("pos", pos_1),
            *inst_attrape,
            ("pos", pos_1),
            *inst_maison,
            ("esc", False),
            ("wait", 1)
        ]

        print(instructions)

        return instructions

    def score(self) -> int:
        return 43  # Score estim�
    
    def get_start_position(self):
        return Position(225, 225, pi/4)
    
class GreenAutoStrat (ListedStrat): # KOBEEE !!!!!!!!!!
    
    def _get_commands(self):

        pos_0 = Position(1750, 2000 - 225, pi/4)
        pos_1 = Position(1650, 2000 - 225, pi/4)
        inst_attrape, pos_2 = attrape_cerise(pos_1, 2)
        inst_maison = retour_maison(pos_1, TeamColor.VERT)

        instructions = [
            ("pos", pos_0),
            ("pos", pos_1),
            *inst_attrape,
            ("pos", pos_1),
            *inst_maison,
            ("esc", False),
            ("wait", 1)
        ]

        print(instructions)

        return instructions

    def score(self) -> int:
        return 43  # Score estim�
    
    def get_start_position(self):
        return Position(225, 2000 - 225, pi/4)
    
class BlueAutoStrat_Experiment (ListedStrat): # KOBEEE !!!!!!!!!!
    
    def _get_commands(self):

        pos_0 = self.get_start_position()
        inst_attrape, pos_1 = attrape_cerise(pos_0, 3)
        inst_maison = retour_maison(pos_1, TeamColor.BLEU)

        instructions = [
            *inst_attrape,
            *inst_maison,
            ("esc", False),
            ("wait", 1)
        ]

        print(instructions)

        return instructions

    def score(self) -> int:
        return 43  # Score estim�
    
    def get_start_position(self):
        return Position(225, 225, pi/4)
    

### --------------------- ###

STRAT_DICT: Dict[str, Type[Strategie]] = {
    "Strategie Bleue aspi'": BlueSuckStrat,
    "Test strat": TestStrat,
    "Homologation Robot 1": HomologationRobot1,
    "Demonstration": Demonstrations,
    "Pré-chargé seulement bleu (60) ⚠️ Cale!": PreloadedOnlyBlue,
    "Pré-chargé seulement vert (60) ⚠️ Cale!": PreloadedOnlyGreen,
    "Verte aspi serie 2": GreenEasyStrat,
    "Bleue aspi serie 2": BlueEasyStrat,
    "Bleue assiette centrale":  BlueCenterStrat,
    "Verte assiette centrale": GreenCenterStrat,
    "🔵 Bleu : plat -> gateau -> aspi -> panier -> coin": BlueAllStraightAction,
    "🟢 Vert : plat -> gateau -> aspi -> panier -> coin": GreenAllStraightAction,
    "🔵 Bleu : Auto-générée [rack 1]": BlueAutoStrat,
    "🟢 Vert : Auto-générée [rack 2]": GreenAutoStrat,
    "🔵 Bleu : Auto-générée [rack 3] [audacieux]": BlueAutoStrat_Experiment,
}
